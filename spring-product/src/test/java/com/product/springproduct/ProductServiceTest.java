package com.product.springproduct;

import com.product.springproduct.model.Product;
import com.product.springproduct.repository.ProductRepository;
import com.product.springproduct.service.ProductService;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {
 @Mock
  ProductRepository productRepository;
 @InjectMocks
  ProductService productService;
 
 @Test
  public void getAllProduct(){
   Mockito.when(productRepository.findAll()).thenReturn(new ArrayList<>());
   List<Product> products = productService.getAllProducts();
   assertEquals(0, products.size());
 }
 @Test
 public void createProduct(){
  Product product = new Product();
  product.setCreditId("12");
  product.setId(12L);
  product.setProductName("test");
  product.setValue(121);
  
  Mockito.when(productRepository.save(product)).thenReturn(product);
  Product result = productService.createProduct(product);
  assertSame(result.getId(), product.getId());
 }
 
 @Test
 public void getProductByCreditId(){
  Product product = new Product();
  product.setCreditId("12");
  product.setId(12L);
  product.setProductName("test");
  product.setValue(121);
  Mockito.when(productRepository.findByCreditId("12")).thenReturn(product);
  Product result = productService.getProductFromCredit("12");
  assertSame(result.getId(), product.getId());
 }
  
}
