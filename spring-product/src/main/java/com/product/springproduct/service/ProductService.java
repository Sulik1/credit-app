package com.product.springproduct.service;

import com.product.springproduct.model.Product;
import com.product.springproduct.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {
    @Autowired
    ProductRepository repository;
    public Product createProduct(Product product){
        repository.save(product);
        return product;
    }
    public List<Product> getAllProducts(){
        List<Product> list = new ArrayList<>();
        for (Product p : repository.findAll()){
         list.add(p);
        }
        return list;
    }
    public Product getProductFromCredit(String creditId){
        return repository.findByCreditId(creditId);
    }
}
