package com.product.springproduct.repository;

import com.product.springproduct.model.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product,Long> {
    Product findByCreditId(String id);

}
