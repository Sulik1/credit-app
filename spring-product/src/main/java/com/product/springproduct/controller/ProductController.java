package com.product.springproduct.controller;

import com.product.springproduct.model.Product;
import com.product.springproduct.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {
    @Autowired
    ProductService productService;
    @PostMapping("/product")
    public Product create(@RequestBody Product product){
        return productService.createProduct(product);
    }
    @GetMapping("/products")
    public List<Product> getAll(){
        return productService.getAllProducts();
    }
    @GetMapping("/products/credit/{id}")
    public Product getProduct(@PathVariable String id){
        return productService.getProductFromCredit(id);
    }
}
