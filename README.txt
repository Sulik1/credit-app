#run
1.Zbudawać jar każdego serwisu mozna użyć komendy mvn clean install
2.Uruchomić docker-compose up

#play
1.Zwracanie wszystkich wniosków kredytowych:
url: http://localhost:8083/credits
metoda: GET

2.Tworzenie wniosku kredytowego
url: http://localhost:8083/credit
metoda: POST
Request body example:
{
"client":{
  "first_name": "first name",
  "pesel": "pesel",
  "surname": "surname"
},
"product":{
  "product_name":"product name"
  "value": 12
},
"credit":{
  "credit_name":"credit name"
}
}
