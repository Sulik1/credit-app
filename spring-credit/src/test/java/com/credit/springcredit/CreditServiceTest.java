package com.credit.springcredit;
import static org.junit.Assert.*;

import com.credit.springcredit.model.Credit;
import com.credit.springcredit.repository.CreditRepository;
import com.credit.springcredit.service.CreditService;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class CreditServiceTest {
  @Mock
  CreditRepository creditRepository;
  @InjectMocks
  CreditService creditService;
  
  @Test
  public void getAll(){
    Mockito.when(creditRepository.findAll()).thenReturn(new ArrayList<>());
    List<Credit> credits = creditService.getAll();
    assertEquals(0, credits.size());
    
  }
  @Test
  public void create(){
    Credit credit = new Credit();
    credit.setCreditName("test");
    credit.setId("1");
    Mockito.when(creditRepository.save(credit)).thenReturn(credit);
    Credit result = creditService.create(credit);
    assertEquals("1", result.getId());
    
  }
  

}
