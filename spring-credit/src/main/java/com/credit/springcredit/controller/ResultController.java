package com.credit.springcredit.controller;

import com.credit.springcredit.model.Result;
import com.credit.springcredit.model.view.ResultView;
import com.credit.springcredit.service.ResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ResultController {
    @Autowired
    ResultService resultService;
    @PostMapping("/credit")
    public String createCredit(@RequestBody Result result){
        return resultService.create(result);
    }
    @GetMapping("/credits")
    public List<ResultView> getResults(){
        return resultService.getAll();
    }
}
