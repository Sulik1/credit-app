package com.credit.springcredit.service;

import com.credit.springcredit.mapper.ClientViewMapper;
import com.credit.springcredit.mapper.CreditViewMapper;
import com.credit.springcredit.mapper.ProductViewMapper;
import com.credit.springcredit.model.Client;
import com.credit.springcredit.model.Credit;
import com.credit.springcredit.model.Product;
import com.credit.springcredit.model.Result;
import com.credit.springcredit.model.view.ResultView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Service
public class ResultService {
    @Autowired
    CreditService creditService;
    @Autowired
    ClientViewMapper clientViewMapper;
    @Autowired
    CreditViewMapper creditViewMapper;
    @Autowired
    ProductViewMapper productViewMapper;

    public String create(Result result){
        String creditId = UUID.randomUUID().toString();
        Credit credit = new Credit();
        credit.setId(creditId);
        credit.setCreditName(result.getCredit().getCreditName());
        Product product = result.getProduct();
        product.setCreditId(credit.getId());
        Client client = result.getClient();
        client.setCreditId(credit.getId());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForEntity("http://client:8082/client",client,Client.class);
        restTemplate.postForEntity("http://product:8081/product",product,Product.class);
        creditService.create(credit);
        return credit.getId();
    }
    public List<ResultView> getAll(){
        RestTemplate restTemplate = new RestTemplate();
        List<ResultView> results = new ArrayList<>();
        List<Credit> credits = creditService.getAll();
        for (Credit credit : credits){
            ResultView result = new ResultView();
            ResponseEntity<Client> clientRest = restTemplate.getForEntity("http://localhost:8082/clients/credit/" + credit.getId(), Client.class);
            Client client = clientRest.getBody();
            ResponseEntity<Product> productRest = restTemplate.getForEntity("http://localhost:8081/products/credit/" + credit.getId(), Product.class);
            Product product = productRest.getBody();
            result.setClientView(clientViewMapper.mapToView(client));
            result.setCreditView(creditViewMapper.mapToView(credit));
            result.setProductView(productViewMapper.mspToView(product));
            results.add(result);

        }

        return results;
    }
}
