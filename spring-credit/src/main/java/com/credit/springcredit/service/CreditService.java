package com.credit.springcredit.service;

import com.credit.springcredit.model.Credit;
import com.credit.springcredit.repository.CreditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class CreditService {
    @Autowired
    CreditRepository repository;
    public Credit create(Credit credit){
        return repository.save(credit);
    }
    
    public List<Credit> getAll(){
        List<Credit> list = new ArrayList<>();
        for (Credit c : repository.findAll()){
            list.add(c);
        }
        return list;
    }
}
