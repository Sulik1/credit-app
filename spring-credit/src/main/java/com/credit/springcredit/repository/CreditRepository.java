package com.credit.springcredit.repository;

import com.credit.springcredit.model.Credit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CreditRepository extends CrudRepository<Credit,Long> {

}
