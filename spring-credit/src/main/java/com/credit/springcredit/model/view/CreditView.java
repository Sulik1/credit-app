package com.credit.springcredit.model.view;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreditView {
    @JsonProperty("credit_name")
    private String creditName;

    public CreditView() {
    }

    public String getCreditName() {
        return creditName;
    }

    public void setCreditName(String creditName) {
        this.creditName = creditName;
    }
}
