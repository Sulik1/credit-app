package com.credit.springcredit.model.view;

public class ResultView {
    private ClientView clientView;
    private ProductView productView;
    private CreditView creditView;

    public ResultView() {
    }

    public ClientView getClientView() {
        return clientView;
    }

    public void setClientView(ClientView clientView) {
        this.clientView = clientView;
    }

    public ProductView getProductView() {
        return productView;
    }

    public void setProductView(ProductView productView) {
        this.productView = productView;
    }

    public CreditView getCreditView() {
        return creditView;
    }

    public void setCreditView(CreditView creditView) {
        this.creditView = creditView;
    }
}
