package com.credit.springcredit.model.view;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ClientView {

    @JsonProperty("first_name")
    private String firstName;
    private String pesel;
    private String surname;

    public ClientView() {
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
