package com.credit.springcredit.model.view;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductView {
    @JsonProperty("product_name")
    private String productName;
    private int value;

    public ProductView() {
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
