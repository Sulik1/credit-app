package com.credit.springcredit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCreditApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCreditApplication.class, args);
	}

}
