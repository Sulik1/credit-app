package com.credit.springcredit.mapper;

import com.credit.springcredit.model.Client;
import com.credit.springcredit.model.view.ClientView;
import org.springframework.stereotype.Service;

@Service
public class ClientViewMapper {
    public ClientView mapToView(Client client){
        ClientView clientView = new ClientView();
        clientView.setFirstName(client.getFirstName());
        clientView.setPesel(client.getPesel());
        clientView.setSurname(client.getSurname());
        return clientView;
    }
}
