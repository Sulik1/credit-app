package com.credit.springcredit.mapper;

import com.credit.springcredit.model.Credit;
import com.credit.springcredit.model.view.CreditView;
import org.springframework.stereotype.Service;

@Service
public class CreditViewMapper {
    public CreditView mapToView(Credit credit){
        CreditView creditView = new CreditView();
        creditView.setCreditName(credit.getCreditName());
        return creditView;
    }
}
