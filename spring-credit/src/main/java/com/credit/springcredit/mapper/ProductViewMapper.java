package com.credit.springcredit.mapper;

import com.credit.springcredit.model.Product;
import com.credit.springcredit.model.view.ProductView;
import org.springframework.stereotype.Service;

@Service
public class ProductViewMapper {
    public ProductView mspToView(Product product){
        ProductView productView = new ProductView();
        productView.setProductName(product.getProductName());
        productView.setValue(product.getValue());
        return productView;
    }
}
