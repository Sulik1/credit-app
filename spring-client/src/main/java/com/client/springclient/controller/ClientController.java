package com.client.springclient.controller;

import com.client.springclient.model.Client;
import com.client.springclient.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ClientController {
    @Autowired
    ClientService clientService;
    @PostMapping("/client")
    public Client create(@RequestBody Client client){
        return clientService.createClient(client);
    }
    @GetMapping("/clients")
    public List<Client> getAll(){
        return clientService.getAll();
    }
    @GetMapping("clients/credit/{id}")
    public Client getClient(@PathVariable String id){
        return clientService.getClientFormCreditId(id);
    }
}
