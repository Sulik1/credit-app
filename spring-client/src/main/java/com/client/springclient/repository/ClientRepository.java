package com.client.springclient.repository;

import com.client.springclient.model.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends CrudRepository<Client,Long> {
    Client findByCreditId(String creditId);
}
