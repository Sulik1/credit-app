package com.client.springclient.service;

import com.client.springclient.model.Client;
import com.client.springclient.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientService {
    @Autowired
    ClientRepository repository;
    public List<Client> getAll(){
        List<Client> list = new ArrayList<>();
        for (Client c : repository.findAll()){
            list.add(c);
        }
        return list;
    }

    public Client createClient(Client client){
        return repository.save(client);
    }
    
    public Client getClientFormCreditId(String creditId){
        return repository.findByCreditId(creditId);
    }
}
