package com.client.springclient;

import com.client.springclient.model.Client;
import com.client.springclient.repository.ClientRepository;
import com.client.springclient.service.ClientService;
import com.fasterxml.jackson.databind.util.ArrayIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceTest {
  
  @Mock
  ClientRepository clientRepository;
  
  @InjectMocks
  ClientService clientService;
  
  @Test
  public void getAllClientsTest(){
    Mockito.when(clientRepository.findAll()).thenReturn(new ArrayList<>());
    List<Client> all = clientService.getAll();
    assertEquals(0, all.size());
  }
  @Test
  public void getClientById(){
    Client client = new Client();
    client.setFirstName("test");
    client.setPesel("123123");
    client.setSurname("test");
    client.setCreditId("121");
    client.setId(12L);
    Mockito.when(clientRepository.findByCreditId("1")).thenReturn(client);
    Client result = clientService.getClientFormCreditId("1");
    assertTrue(result.getFirstName().equals(client.getFirstName()));
    
  }
  @Test
  public void CreateUser(){
    Client client = new Client();
    client.setFirstName("test");
    client.setPesel("123123");
    client.setSurname("test");
    client.setCreditId("121");
    client.setId(12L);
    Mockito.when(clientRepository.save(client)).thenReturn(client);
    Client result = clientService.createClient(client);
    assertTrue(result.getFirstName().equals(client.getFirstName()));
  }

}
